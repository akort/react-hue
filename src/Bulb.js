import React from 'react';

class Bulb extends React.Component {
	
	state = {
		isChecked: this.props.isChecked
	}

	render() {

		return (
			<div>
				{this.props.bulbstate !== undefined ? 
				<div>
					<div>{"Bulb name: " + this.props.name} </div>
					<div>{"Hue: " + this.props.bulbstate.hue} </div>
					<div>{"On: " + this.props.bulbstate.on}</div>
					<div>{"Brightness: " + this.props.bulbstate.bri} </div>
					<div>{"Saturation: " + this.props.bulbstate.sat} </div>
					<br></br>
				</div>:
				null}
			</div>
		)
	}
  }
  export default Bulb