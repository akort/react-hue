import './App.css'

import Info from "./Info"
import React, { useState } from "react"
import { HuePicker } from 'react-color'

import Box from '@mui/material/Box'
import Slider from '@mui/material/Slider'


function App() {
  const [color, setColor] = useState({"h":1,"s":1,"v":1,"a":1})

  const [saturation, setSaturation] = React.useState(50)
  const [brightness, setBrightness] = React.useState(50)
  
  const handleChange = (event, newValue) => {
    const correctedValue = parseInt(newValue*2.54)
    event.target.name === "SaturationSlider" ? setSaturation(correctedValue): setBrightness(correctedValue)
  };
  

  const colorpickerStyle = {
    width: "100%",
    height: "10%",
    position: "fixed",
    top: "2%",
    left: "00%",
    zIndex: 5
    }
  const saturationpickerStyle = {
    position:"absolute",
    top: "70%",
    left:"1%",
    width: "40%",
    zIndex: 5
  }

  const brightnesspickerStyle = {
    position:"absolute",
    top: "70%",
    left:"51%",
    width: "40%",
    zIndex: 5
  }

  const topBarBackground = {
    position:"fixed",
    backgroundColor:"#282c34",
    top: "0%",
    left:"0%",
    width: "100%",
    height: "20%",
    zIndex: 2
  }

  return (
    <div className="App">
      <header className="App-header">
     
        <Info color={color} saturation={saturation} brightness={brightness}></Info>
        <div style={topBarBackground}/>
        <div style={colorpickerStyle}>
          <HuePicker         
          color={color}
          width={"100%"}
          height="50%"
          onChangeComplete={color => {
            setColor(color.hsv);
          }}
          />
          <Box width={300} style={saturationpickerStyle}> Saturation
          <Slider defaultValue={50} aria-label="Saturation" valueLabelDisplay="auto" saturation={saturation} onChange={handleChange} name={"SaturationSlider"}/>
          </Box>

          <Box width={300} style={brightnesspickerStyle}> Brightness
          <Slider defaultValue={50} aria-label="Brightness" valueLabelDisplay="auto" brightness={brightness} onChange={handleChange} name={"BrightnessSlider"} />
          </Box>
        </div>
      </header>
    </div>
  );
}

export default App;
