import axios from "axios"

const bridgeIp = process.env.REACT_APP_BRIDGE_IP
const apiKey = process.env.REACT_APP_HUE_API_KEY

const baseUrl = `http://${bridgeIp}/api/${apiKey}/`



const getMetadata = async () => {
	const request = await axios.get(baseUrl+"lights")
	return request
}
const setLight = async(id,on,sat,bri,hue) => {
	const request = await axios.put(`${baseUrl}lights/${id}/state`,{
		"on":on,
		"sat":sat,
		"bri":bri,
		"hue":hue
	})
	return request
}
const getGroups = async() =>{
	const request = await axios.get(`${baseUrl}groups`)
	return request
}

const changeScene = async(id,sceneId,bri) =>{
	const request = await axios.put(`${baseUrl}groups/${id}/action`,{
		"scene": sceneId
	})
	const request2 = await axios.put(`${baseUrl}groups/${id}/action`,{
		"bri": bri // Send 2 requests because API is stupid
	})
	return request
}

const getScene = async(id) => {
	const request = await axios.get(`${baseUrl}scenes/${id}`)
	return request
}

const getScenes = async() =>{
	const request = await axios.get(`${baseUrl}scenes`)
	return request
}

export default {getMetadata,setLight,getGroups,changeScene,getScene,getScenes}