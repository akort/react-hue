import React from 'react';

class Scene extends React.Component {
	render() {
		const scenestate = this.props.scenestate
		return (
			<div>
				{this.props.scenestate !== undefined ? 
				<div>
					<div>{"Scene name: " + scenestate.name} </div>
					<div>{"Hue: " } </div>
					<div>{"On: " }</div>
					<div>{"Brightness: " } </div>
					<div>{"Saturation: " } </div>
					<div>{"id: " + this.props.id}</div>
					<br></br>
				</div>:
				null}
			</div>
		)
	}
  }
  export default Scene