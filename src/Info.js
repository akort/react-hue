import React from 'react';
import api from "./api"
import Bulb from "./Bulb"
import LightGroup from "./LightGroup"
import Scene from "./Scene"

class Info extends React.Component {

	constructor() {
		super()
		this.state = {
			bulbs: {},
			lightGroups: [],
			scenes:[],
			selectedGroupId:""
		}
	}

	componentDidMount() {

		api.getMetadata().then(response =>{
			let data = response.data
			

			const bulbs = Object.entries(data).map(bulb => {
				return({id:bulb[0],...bulb[1],isChecked:false}) // Add id and isChecked field to each lightbulb
			})
			this.setState({bulbs})
		})

		api.getGroups().then( response => {
			let data = response.data
			const lightGroups = Object.entries(data).map(group => {
				return({id:group[0],...group[1],isSelected:false}) // Add id and isSelected to each lightgroup
			})
			this.setState({lightGroups})
		})
		api.getScenes().then(response => {
			let sceneIds = Object.keys(response.data)
			let scenePromises = sceneIds.map(id => {
				return(
					api.getScene(id).then(response => {
						return {...response.data,id}
					})
				)
			}
			)
			Promise.all(scenePromises).then(scenes=> this.setState({scenes}))

		})

	}
	componentDidUpdate(){
		const hueMax = 65536
		const hueMaxHuePicker = 360 
		const hueCorrection = hueMax/hueMaxHuePicker
		const hue = parseInt(this.props.color.h*hueCorrection)
		const saturation = parseInt(this.props.saturation) // Saturation is between 0 to 254
		const brightness = parseInt(this.props.brightness)
		
		const bulbs = this.state.bulbs.map(x =>{ // Set light's values so that state can keep up
		return(	x.isChecked === false ? x : (api.setLight(x.id,true,saturation,brightness,hue),{...x,state:{...x.state,sat:saturation,hue,bri:brightness}}))
		})

		return(JSON.stringify(bulbs) === JSON.stringify(this.state.bulbs) ? "" : this.setState({bulbs})) // Refresh state
	}

	
	render() {
		const groupInfoStyle = {
			color: "black",
			backgroundColor: "Coral",
			fontFamily: "Arial",
			position: "absolute",
			width: "30%",
			top: "25%",
			left: "0%",
		  }
		  const bulbInfoStyle = {
			color: "black",
			backgroundColor: "grey",
			fontFamily: "Arial",
			position: "absolute",
			width: "30%",
			top: "25%",
			left: "30%"
		  }
		  const sceneStyle = {
			color: "black",
			backgroundColor: "Coral",
			fontFamily: "Arial",
			position: "absolute",
			width: "30%",
			top: "25%",
			left: "60%",
		  }

		const setSelectedGroup = (group) => {
			const bulbs= this.state.bulbs.map(x => {
				return({...x,isChecked:false})
			})
			this.setState({bulbs,selectedGroupId: group.id})
		}
		const lightGroupInfo = this.state.lightGroups.map((lightGroup,i)=> {
			return(
				<div style={{border:"solid"}} onClick={()=>setSelectedGroup(lightGroup)}>
					<LightGroup lightGroupstate={lightGroup.action}   name={lightGroup.name} selected={lightGroup.id===this.state.selectedGroupId}></LightGroup>
				</div>
			)
		})
		const handleCheckboxChange = (e,id) => {
			const bulbs= this.state.bulbs.map(x => x.id ===id ? {...x,isChecked:!x.isChecked}:{...x})
			this.setState({bulbs})

		  }

		const bulbInfo = Object.values(this.state.bulbs).map((bulb,i) => {
			const checkBoxstyle  = {
				position: "relative",
				top: "-100px",
				left: "40%",
				transform: "scale(2.5)"
			}
			return (
				<div style={{border:"solid"}}>
					<Bulb bulbstate={bulb.state} name={bulb.name} isChecked={bulb.isChecked} id={bulb.id}></Bulb>
        				<input style={checkBoxstyle}type="checkbox" checked={bulb.isChecked} onChange={e=>handleCheckboxChange(e,bulb.id)}/>
				</div>

			)
		})

		const handlechangeScene = (id) => {
			api.changeScene(this.state.selectedGroupId,id,this.props.brightness)
		}

		const scenes = this.state.scenes.map((scene,i) => {
			return (
				<div style={{border:"solid"}} name={scene.id} onClick={() => handlechangeScene(scene.id)}>
					<Scene scenestate={scene} id={scene.id}> </Scene>
				</div>
			)
		})
		const handleClick = (e) => {
			if(e.target.name==="SelectAll") {
				const bulbs = this.state.bulbs.map(x => {
					return({...x,isChecked:!x.isChecked})
				})
				this.setState({bulbs})
			} else { // Turn off selected bulbs
				const bulbs = this.state.bulbs.filter(x => {
					return(x.isChecked === true)
				})
				bulbs.forEach(x => api.setLight(x.id,false))
			}
		}

		const buttonStyle = {
			position:"fixed",
			zIndex: 1000,
			top: "9%",
			left: "44%"
		}
		const turnOffButtonStyle = {
			position: "fixed",
			zIndex: 1000,
			top: "17%",
			left: "43%"
		}



	  return (
		<div>
			<button name={"SelectAll"} style={buttonStyle} onClick={handleClick}>Select all</button>
			<button name={"TurnOff"} style={turnOffButtonStyle} onClick={handleClick}>Turn off selected</button>
			<div style={bulbInfoStyle}>{Object.keys(this.state.bulbs).length >= 1 ? bulbInfo : null}</div>
			<div style={groupInfoStyle}>{this.state.lightGroups.length >= 1 ? lightGroupInfo : null}</div>
			<div style={sceneStyle}> {this.state.scenes.length >= 1 ? scenes : null} </div>
		</div>
	  );
	}
  }
  export default Info