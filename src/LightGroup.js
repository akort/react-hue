import React from 'react';

class LightGroup extends React.Component {
	render() {
		const selectedStyle = {
			backgroundColor: "lightcoral"
		}
		const notSelectedStyle = {
		}

		return (
			<div>
				{this.props.lightGroupstate !== undefined ? 
				<div style={this.props.selected ? selectedStyle : notSelectedStyle}>
					<div>{"Group name: " + this.props.name} </div>
					<div>{"Hue: " + this.props.lightGroupstate.hue} </div>
					<div>{"On: " + this.props.lightGroupstate.on}</div>
					<div>{"Brightness: " + this.props.lightGroupstate.bri} </div>
					<div>{"Saturation: " + this.props.lightGroupstate.sat} </div>
					<br></br>
				</div>:
				null}
			</div>
		)
	}
  }
  export default LightGroup