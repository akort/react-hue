
New Node version requires: 

`export NODE_OPTIONS=--openssl-legacy-provider`


Create .env file in the project root and add "REACT_APP_HUE_API_KEY=" "REACT_APP_BRIDGE_IP=" fields

e.g. 

REACT_APP_HUE_API_KEY=khrag7wLKGIVHrRLIQoHoHfTl9JsnlHdBnAwhu45

REACT_APP_BRIDGE_IP=10.10.10.10

### `yarn install`

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


![React-hue-ui](react-hue-ui.png "React-hue-ui")


### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

## Static server from [deployment](https://facebook.github.io/create-react-app/docs/deployment)

`npm run build`

`npm install -g serve`

`serve -s build -l 3030`


Here is a way to install packages globally for a given user.
1. Create a directory for global packages

mkdir "${HOME}/.npm-packages"

2. Tell npm where to store globally installed packages

npm config set prefix "${HOME}/.npm-packages"

3. Ensure npm will find installed binaries and man pages

Add the following to your .bashrc/.zshrc:

NPM_PACKAGES="${HOME}/.npm-packages"

export PATH="$PATH:$NPM_PACKAGES/bin"

export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"

Reload bashrc
source ~/.bashrc


See: https://github.com/sindresorhus/guides/blob/main/npm-global-without-sudo.md for installing serve globally (and remember to reload .bashrc)